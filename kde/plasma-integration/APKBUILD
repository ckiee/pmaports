# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-integration
pkgver=5.15.2
pkgrel=0
pkgdesc="Qt Platform Theme integration plugins for the Plasma workspaces"
arch="all"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="LGPL-3.0"
depends="ttf-hack"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev qt5-qtquickcontrols2-dev kconfig-dev kconfigwidgets-dev ki18n-dev kiconthemes-dev kio-dev knotifications-dev kwayland-dev kwidgetsaddons-dev kwindowsystem-dev kconfigwidgets-dev breeze-dev libxcursor-dev"
source="https://download.kde.org/stable/plasma/$pkgver/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Requires X11 to be running

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_DISABLE_FIND_PACKAGE_FontHack=true
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="e6c26f01b595ca99da84fbc2ce9b9e558594c2c1914bea8d7fa6bcb6c5a763dd990f59d33436b44a246415d03f9a9bf0c2ff8a07a6aa59c7c31d12658df5ca00  plasma-integration-5.15.2.tar.xz"
