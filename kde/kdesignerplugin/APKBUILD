# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdesignerplugin
pkgver=5.55.0
pkgrel=0
pkgdesc="Integration of Frameworks widgets in Qt Designer/Creator"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends_dev="kcoreaddons-dev kconfig-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev kdoctools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring running X11

build() {
	cd "$builddir"
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKF5WebKit_FOUND=OFF
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}


package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="fffb687a5b1e60ad9c41b825a907f18d23822a336133b855fbcd3be5f679b8dab4f36bc6bde48c4bfba9b402d998824b208d9670d697770b85088119e14f4574  kdesignerplugin-5.55.0.tar.xz"
