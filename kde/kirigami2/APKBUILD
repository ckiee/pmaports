# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kirigami2
pkgver=5.55.0
pkgrel=0
pkgdesc="A QtQuick based components set"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL"
depends="qt5-qtgraphicaleffects"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev qt5-qtquickcontrols2-dev kpackage-dev kcoreaddons-dev kservice-dev kconfig-dev kwindowsystem-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-lang"
options="!check"
builddir="$srcdir/$pkgname-$pkgver/build"

prepare() {
	mkdir "$builddir"
}

build() {
	cd "$builddir"
	cmake .. \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DKDE_INSTALL_LIBDIR=lib \
		-DBUILD_EXAMPLES=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="b63521e1d5e057f40299b3e54791b7bc408a30757ba6261c9a92ff76314e12e008b09bbe417b0c8d6752ee3b962c7dfd188f474e054572a4871d3ddfcf3ea1e9  kirigami2-5.55.0.tar.xz"
