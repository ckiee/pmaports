# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kservice
pkgver=5.55.0
pkgrel=0
pkgdesc="Advanced plugin and service introspection"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends=""
depends_dev="kcrash-dev kconfig-dev kdbusaddons-dev ki18n-dev kcoreaddons-dev"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev doxygen qt5-qttools-dev flex-dev bison"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check"
builddir="$srcdir/$pkgname-$pkgver/build"

prepare() {
	mkdir "$builddir"
}

build() {
	cd "$builddir"
	cmake .. \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="e2ab490edbbe0ff4a96253eccb8356c858714cc265b854244d350f0b28c5ceb096494d7b1ac285d2b854d135171b40de247f49d866a53a5528182c6c72fbc68c  kservice-5.55.0.tar.xz"
