# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=attica-qt
_pkgname="${pkgname/-qt/}"
pkgver=5.55.0
pkgrel=0
arch="all"
pkgdesc="Qt5 library that implements the Open Collaboration Services API"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$_pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # ProviderTest::testFetchInvalidProvider() fails with: Could not fetch provider
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="09fc4c00a4543af82f3fe4d8bddf16fbb8675545623d9f851714b03fca4b7ba18643f471f251ccbcb577824ae1b9a680383094c884f2f3ffd07a3067a7afa0cb  attica-5.55.0.tar.xz"
