# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=qtmir
pkgver=0_git20190129
_commit="7ba43e5310f39219ee01e73976d44e4b581fc4df"
pkgrel=0
pkgdesc="QPA plugin to make Qt a Mir server"
arch="x86_64"
url="https://unity8.io"
license="LGPL-3.0"
depends=""
makedepends="$depends_dev cmake cmake-extras gtest-dev mir-dev process-cpp-dev qt5-qtdeclarative-dev qt5-qtsensors-dev ubuntu-app-launch-dev gsettings-qt-dev libqtdbustest libqtdbusmock content-hub-dev valgrind-dev url-dispatcher-dev"
source="$pkgname-$_commit.tar.gz::https://github.com/ubports/$pkgname/archive/$_commit.tar.gz"
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare
	mkdir -p "$builddir"/build

	# Don't build tests, they require Mir to be built with tests which is disabled
	truncate -s 0 "$builddir"/tests/CMakeLists.txt
}

build() {
	cd "$builddir"/build
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="69df637dbe88041468609ccdf9d428f5ca9fb0a67bb2b566f9827d49e47857242525b576ca8e528a374435c6dfadc3e2aa190d5dbb305f04d013c9cb981cac3b  qtmir-7ba43e5310f39219ee01e73976d44e4b581fc4df.tar.gz"
